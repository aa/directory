const path = require('path');
// const UglifyJSPlugin = require('uglifyjs-webpack-plugin')
module.exports = {
    entry: "./src/feedplayer.js",
    mode: "development",
    output: {
        filename: "feedplayer.js",
        path: path.resolve(__dirname, 'dist'),
        library: "feedplayer"
    }
    // plugins: [
    //     new UglifyJSPlugin()
    // ]
}