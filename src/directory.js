var d3 = require("d3-selection"),
	loadscript = require("./loadscript.js"),
	droptoupload = require("./droptoupload.js"),
	make_controller = require("./controller.js"),
	cookie = require("./cookie.js");
var markdown = require( "markdown" ).markdown;

// polyfills
require("./trim.js");

var controller;

function normalize_url (url) {
	// aaaaaaaaaaaaaargh, python capitalizes % encoded values
	// apache directory listing seem to be lower cased
	// RegEx to the rescue
	return url.replace(new RegExp("%[a-f0-9][a-f0-9]", "i"), x => x.toUpperCase());
}

/* Binds initial data items to each tr in the table with url == a[href] */
function init_table_data() {
	var tr = d3.selectAll("table tbody tr");
	tr = tr.filter( (d, i) => (i>=3 && i < tr.size()-1) );
	tr.attr("class", "file");
	var data = tr.nodes().map(x => ({
		'url' : normalize_url(x.querySelector("td:nth-child(2) a[href]").pathname),
		'last-modified': x.querySelector("td:nth-child(3)").textContent.trim(),
		'size': x.querySelector("td:nth-child(4)").textContent.trim()
	}));
	tr.data(data);
	//console.log("init_table_data", data);
}

function set_default_data (selection) {
	var files = d3.selectAll("table tbody tr.file");
	files.each (d => {
		var lurl = d.url.toLowerCase();
		if (! d.type ) {
			if (lurl.endsWith(".mp3") || lurl.endsWith(".ogg") || lurl.endsWith(".wav")) {
				d.type = "audio";
			} else if (lurl.endsWith("/")) {
				d.type = "folder";
			}
		}
	})
}

function get_thumb_url (d) {
	if (!d.formats) return;
	let links = d.formats
		.filter(x => x.type=="image")
		.sort((x, y) => (x.size - y.size));
	if (links.length) {
		return links[0].url;
	}
}

async function read_metadata(json_url) {
	try {	
		var resp = await fetch(json_url),
			data = await resp.json();
		console.log("got index.json", data);
		var tr = d3.selectAll("table tbody tr.file");
		var update = tr.data(data.children, d => decodeURI(d.url)),
			enter = update.enter(),
			exit = update.exit();
		
		// console.log("update", update, update.size());
		// should be no enter/exits...
		enter.each(function (d, i) { console.log("warning: enter", i, d.url); });
		exit.each(function (d, i) { console.log("warning: exit", i, d.url); });

		// Integrate data
		update.each(function (d, i) {
			if (d.description) {
				d3.select(this).select("td:nth-child(5)").html( markdown.toHTML( d.description ));
			}
		});
		update.filter(get_thumb_url).select("img").attr("src", d=>get_thumb_url(d)).classed("thumbnail", true);

		return data;
	} catch (error) {
		console.log("error", error);
		return;
	}
}

function activate_table () {
	var tr = d3.selectAll("tr.file");
	tr.on("click", function (d) {
		if (d.type != "folder") {
			d3.event.preventDefault();
			controller.play.call(this, d);
		}
	});
}

async function init () {
	var aa_auth = cookie.get("aa_auth");
	// console.log("aa_auth", aa_auth, aa_auth=="true");
	controller = make_controller();
	// Initialize data, prepare for join

	init_table_data();
	set_default_data();
	var files = d3.selectAll("table tbody tr.file");
	var data = files.data();
	// console.log("enriched data", data);
	// read + join with stored metadata
	//data = await read_metadata(".aa/index.json");
	data = await read_metadata("/cgi-bin/directory.cgi?f=json&u="+window.location.pathname);
	if (get_thumb_url(data)) {
		d3.select(document.body).insert("img", "table").attr("class", "thumbnail").attr("src", get_thumb_url(data));
	}
	var description = d3.select(document.body).insert("p", "table").attr("class", "description");
	if (data.description) {
		description.html(markdown.toHTML(data.description));
	}
	activate_table();
}

document.addEventListener("DOMContentLoaded", init);

//  https://github.com/marcj/css-element-queries
//var ElementQueries = require('css-element-queries/src/ElementQueries');
// attaches to DOMLoadContent and does anything for you
//ElementQueries.listen();
