var d3 = require("d3-selection");


function $button (parent, textContent) {
	var ret = document.createElement("button");
	ret.textContent = textContent;
	parent.appendChild(ret);
	return ret;
}

/* Need abstraction: play [{} data + elt] + index ... and events for observers */
function controller () {
	var elt = d3.select(document.body)
		  .append("div")
		  .attr("class", "aacontroller"),
		ret = {elt: elt},
		edit_iframe_div = elt.append("div")
			.attr("id", "edit_iframe_div"),
		edit_iframe,
		contents = elt.append("div")
			.attr("class", "contents"),
		playbutton = contents.append("button")
			.text("play")
			.on("click", d => {
				if (current_media.paused) {
					current_media.play();
				} else {
					current_media.pause();
				}
			}),
		timeslider = contents.append("input")
			.attr("type", "range")
			.attr("min", 0)
			.attr("max", 10000)
			.attr("step", 1)
			.on("mousedown", x=> {
				dragging = true;
				if (!current_media.paused) {
					current_media.pause();
					playAfterDrag = true;
				} else {
					playAfterDrag = false;
				}
			})
			.on("mouseup", x=> {
				if (dragging) {
					// console.log("stop dragging");
					dragging = false;
					if (current_media.duration) {
						var curtime = (timeslider.node().value / 10000) * current_media.duration;
						current_media.currentTime = curtime;
						if (playAfterDrag) {
							current_media.play();
						}
					}
				}
			}),
			edit_buttons = contents.append("span")
				.attr("class", "edit_buttons"),
			edit_button = edit_buttons.append("button")
				.text("edit")
				.on("click", d=> {
					var open = edit_iframe_div.classed("open"),
						newstate = !open;
					if (newstate) {
						edit_url(current_url ? current_url : window.location.pathname);
					} else {
						edit_iframe_div.classed("open", false);
					}
				}),
			audio,
			video,
			current_media,
			image,
			dragging = false,			
			playAfterDrag = false,
			current_url,
			editing_url;

	function edit_url (use_url) {
		if (use_url && editing_url != use_url) {
			if (edit_iframe === undefined) { init_edit_iframe() };
			edit_iframe.attr("src", "/cgi-bin/directory.cgi?f=annotate&u="+encodeURIComponent(use_url));
			editing_url = use_url;
		}		
		edit_iframe_div.classed("open", true);
	}

	function init_edit_iframe () {
		edit_iframe = edit_iframe_div
			.append("iframe")
			.attr("id", "edit_iframe");
	}
	
	function init_audio () {
		audio = document.createElement("audio");
		audio.addEventListener("play", x => {
			playbutton.text("pause");
		});
		audio.addEventListener("pause", x => {
			playbutton.text("play");
		});
		audio.addEventListener("timeupdate", x => {
			if (dragging) { return; }
			if (audio.duration) {
				var curval = (audio.currentTime / audio.duration) * 10000;
				timeslider.value = curval;
			}
		});
		audio.addEventListener("ended", x=> {
			console.log("audio.ended");
			var next = d3.select("tr.file.playing + tr.file");
			console.log("next");
			if (next) {
				play.call(next.node(), next.datum());
			}
		})
	}

	function init_video () {
		video = document.createElement("video");
		video.setAttribute("controls", "controls");
		video.setAttribute("autoplay", "autoplay");
		contents.node().appendChild(video);
		video.addEventListener("play", x => {
			playbutton.text("pause");
		});
		video.addEventListener("pause", x => {
			playbutton.text("play");
		});
		video.addEventListener("timeupdate", x => {
			// console.log("timeupdate", video.duration);
			if (dragging) { return; }
			if (video.duration) {
				var curval = (video.currentTime / video.duration) * 10000;
				timeslider.node().value = curval;
			}
		});
		video.addEventListener("ended", x=> {
			console.log("video.ended");
			var next = d3.select("tr.file.playing + tr.file");
			console.log("next");
			if (next) {
				play.call(next.node(), next.datum());
			}
		})
	}

	// login.addEventListener("click", x=> {
	// 	loadscript.loadscript("/cgi-bin/aa/droptoupload.cgi?javascript").then(x=> {
	// 		console.log("loaded droptoupload");
	// 	});
	// });

	// console.log("controller", div);
	function play (d) {
		current_url = d.url;
		if (edit_iframe_div.classed("open")) {
			edit_url(current_url);
		}
		console.log("play", this, current_url, d);
		d3.selectAll(".playing").classed("playing", false);
		d3.select(this).classed("playing", true);
		if (d.type == "audio") {
			play_audio.call(this, d);
		} else if (d.type == "video") {
			play_video.call(this, d);
		}
	}
	ret.play = play;

	function play_audio (d) {
		if (audio === undefined) { init_audio(); }
		current_media = audio;
		if (video && !video.paused) { video.pause() }
			// hide the video?
		audio.src = d.url;
		audio.play();		
	}
	ret.play_audio = play_audio;

	function play_video (d) {
		console.log("play_video", d);
		if (video === undefined) { init_video(); }
		current_media = video;
		if (audio && !audio.paused) { audio.pause() }
		video.src = d.url;
		video.play();		
	}
	ret.play_video = play_video;

	return ret;
}
module.exports = controller;
