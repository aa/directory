
dist/directory.js: src/directory.js src/*.js
	node_modules/.bin/webpack-cli --config directory.config.js

dist/feedplayer.js: src/feedplayer.js src/*.js
	node_modules/.bin/webpack-cli --config feedplayer.config.js

upload: dist/directory.js dist/directory.css
	# SERVER
	cp dist/directory.js dist/directory.css ~/mnt/1/public_html/lib/directory/
	cp cgi-bin/*.cgi cgi-bin/*.py ~/mnt/1/cgi-bin
	# LOCAL
	cp dist/directory.js dist/directory.css /var/www/html/lib/directory/
	cp cgi-bin/*.cgi cgi-bin/*.py /usr/lib/cgi-bin
