const path = require('path');
// const UglifyJSPlugin = require('uglifyjs-webpack-plugin')
module.exports = {
    entry: "./src/directory.js",
    mode: "development",
    output: {
        filename: "directory.js",
        path: path.resolve(__dirname, 'dist'),
        library: "directory"
    }
    // plugins: [
    //     new UglifyJSPlugin()
    // ]
}