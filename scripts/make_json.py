import os
import json
from urllib.parse import quote as urlquote


files = os.listdir(".")
files = [x for x in sorted(files) if not x.startswith(".")]
data = {}
data['children'] = children = []

def decorate_link (path):
	mtype = ""
	rel = "alternate"
	if path.endswith(".thumb.jpg"):
		mtype = "image/jpeg"
		rel = "thumb"
	elif path.endswith(".gif"):
		mtype = "image/gif"
	return {'url': urlquote(path), 'type': mtype, 'rel': rel}

for f in files:
	d = {}
	d['url'] = f
	base, ext = os.path.splitext(f)
	t = os.path.join(".aa", f)
	if os.path.exists(t) and os.path.isdir(t):
		d['links'] = [decorate_link(os.path.join(".aa", f, x)) for x in os.listdir(t)]
	children.append(d)
print (json.dumps(data, indent=2))

