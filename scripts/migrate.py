import os

files = os.listdir(".")
files.sort()

def mv (a, b):
	print ("Move {0} to {1}".format(a, b))
	try:
		os.makedirs(os.path.split(b)[0])
	except OSError as e:
		pass
	os.rename(a, b)

def migrate (f, ext):
	base, fext = os.path.splitext(f)
	t = base + ext
	if os.path.exists(t):
		newpath = os.path.join(".aa", f, base + ext)
		mv(t, newpath)

for f in files:
	print (f)
	if f.endswith(".webm"):
		migrate(f, ".thumb.gif")
		migrate(f, ".thumb.jpg")
	else:
		t = os.path.join(".thumbs", f+".jpg")
		if os.path.exists(t):
			base, fext = os.path.splitext(f)
			newpath = os.path.join(".aa", f, base + ".thumb.jpg")
			mv(t, newpath)

