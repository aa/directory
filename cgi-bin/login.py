from base64 import b64encode as encode, b64decode as decode
import binascii

from aa_password import PASSWORD


def read_cookie(env, name):
    cookiestr = env.get("HTTP_COOKIE", "")
    if cookiestr:
        for cookie in cookiestr.split(';'):
            (key, value ) = cookie.strip().split('=');
            if key == name:
                return value

def authenticate (env):
    aa_auth = read_cookie(env, "aa_auth")
    if aa_auth:
        # attempt to authorize
        try:
            if decode(aa_auth).decode("utf-8") == PASSWORD:
                return True
        except binascii.Error:
            return False
