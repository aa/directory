import os, json
from urllib.parse import quote as urlquote, unquote as urlunquote, urlencode

def get_current_url (environ):
    request_scheme = environ.get("REQUEST_SCHEME")
    server_name = environ.get("SERVER_NAME")
    # http_host = environ.get("HTTP_HOST")
    server_port = int(environ.get("SERVER_PORT", "80"))
    request_uri = environ.get("REQUEST_URI")
    if server_port != 80:
        return "{0}://{1}:{2}{3}".format(request_scheme, server_name, server_port, request_uri) 
    else:
        return "{0}://{1}{2}".format(request_scheme, server_name, request_uri) 

def redirect_to(url):
    print ("Location: {0}".format(url))
    print ()
    sys.exit(0)

# https://github.com/achillean/shodan-python/issues/39
def humanize_bytes(bytes, precision=1):
    """Return a humanized string representation of a number of bytes.
    >>> humanize_bytes(1)
    '1 byte'
    >>> humanize_bytes(1024)
    '1.0 kB'
    >>> humanize_bytes(1024*123)
    '123.0 kB'
    >>> humanize_bytes(1024*12342)
    '12.1 MB'
    >>> humanize_bytes(1024*12342,2)
    '12.05 MB'
    >>> humanize_bytes(1024*1234,2)
    '1.21 MB'
    >>> humanize_bytes(1024*1234*1111,2)
    '1.31 GB'
    >>> humanize_bytes(1024*1234*1111,1)
    '1.3 GB'
    """

    if bytes == 1:
        return '1 byte'
    if bytes < 1024:
        return '%.*f %s' % (precision, bytes, "bytes")
   
    suffixes = ['KB', 'MB', 'GB', 'TB', 'PB']
    multiple = 1024.0    #.0 force float on python 2
    for suffix in suffixes:
        bytes /= multiple
        if bytes < multiple:
            return '%.*f %s' % (precision, bytes, suffix)

def index_json_path (path):
    return os.path.join(path, ".aa", "index.json")

def load_index_json (path):
    try:
        with open(index_json_path(path)) as f:
            return json.load(f)
    except OSError:
        return {}
    except json.decoder.JSONDecodeError:
        return {}

def file_formats_path (path, ensureCreate=False):
    if os.path.isfile(path):
        parent, filename = os.path.split(path)
        ret = os.path.join(parent, ".aa", filename)
    else:
        ret = os.path.join(path, ".aa", ".files")
    if ensureCreate:
        try:
            os.makedirs(ret)
        except OSError:
            pass
    return ret

def calc_formats_json (path, file_normalized_url):
    """ Check the .aa/FILENAME folder for this path's derived/formats files.
        Return a list suitable for JSON inclusion as value for the formats key """
    fpath = file_formats_path (path)
    if os.path.isdir(fpath):
        files = os.listdir(fpath)
        files.sort()
        parent, filename = os.path.split(file_normalized_url)
        ret = []
        for x in files:
            fp = os.path.join(fpath, x)
            d= {}
            d['url'] = parent+"/.aa/"+filename+"/"+urlquote(x)
            dtype = guess_item_type(fp)
            if dtype:
                d['type'] = dtype
            d['size'] = os.path.getsize(fp)
            ret.append(d)
        # return [item_datum(os.path.join(fpath, x), parent_nurl, x, is_format=True) for x in files]
        return ret

def guess_item_type (name):
    ext = os.path.splitext(name)[1].lower()[1:]
    if ext in ["ogg", "mp3", "wav"]:
        return "audio"
    elif ext in ["webm", "mp4", "mpv", "ogv"]:
        return "video"
    elif ext in ["pdf"]:
        return "pdf"
    elif ext in ["png", "jpg", "jpeg", "svg", "bmp"]:
        return "image"
    elif ext in ["html"]:
        return "html"

def item_datum (fullpath, normalized_url, filename, is_format=False):
    item = {}
    fp = os.path.join(fullpath, filename)
    if os.path.isdir(fp):
        item['url'] = normalized_url + urlquote(filename) + "/"
        item['type'] = "folder"
        item['info'] = "/cgi-bin/directory.cgi?"+urlencode({'f': 'json', 'u': item['url']})
        item['edit'] = "/cgi-bin/directory.cgi?"+urlencode({'f': 'annotate', 'u': item['url']})

        if not is_format:
            formats = calc_formats_json(fp, item['url']+".files")
            if formats:
                item['formats'] = formats
        # attempt to read data from inner user json file
        ddata = load_index_json(fp)
        if ddata:
            for key in ddata:
                if key != "children":
                    item[key] = ddata[key]
    else:
        item['url' ] = normalized_url + urlquote(filename)
        item['edit'] = "/cgi-bin/directory.cgi?"+urlencode({'f': 'annotate', 'u': item['url']})
        itype = guess_item_type(item['url'])
        if itype:
            item['type'] = itype
        item['size'] = os.path.getsize(fp)
        if not is_format:
            formats = calc_formats_json(fp, item['url'])
            if formats:
                item['formats'] = formats

    return item

def listdir (fullpath, normalized_url):
    ret = {}
    ret['url'] = normalized_url
    ret['type'] = "folder"
    formats = calc_formats_json(fullpath, normalized_url+".files")
    if formats:
        ret['formats'] = formats
    items = os.listdir(fullpath)
    items.sort()
    items = [x for x in items if not x.startswith(".")]
    ret['children'] = [item_datum(fullpath, normalized_url, filename) for filename in items]
    return ret

def merge_data (filesystem, user):
    """ takes the output of listdir + load_index_json & merges them,
        keeping the structure / elements of the filesystem with any additions from user shadowing
        modifies: filesystem object
    """
    def merge (a, b):
        for key in b:
            if key != "children" and key != "url":
                a[key] = b[key]
    merge(filesystem, user)
    children_by_url = {}
    if 'children' in filesystem:
        for c in filesystem['children']:
            if 'url' in c:
                children_by_url[c['url']] = c
    if 'children' in user:
        for c in user['children']:
            if 'url' in c:
                achild = children_by_url.get(c['url'])
                if achild:
                    merge(achild, c)


def send_form(item, fs_item, messages):
    # # add this data to the appropriate index.json ... creating if necessary
    # name = os.path.splitext(filename)[0].replace("_", " ")
    # title = fs.getvalue("title", "")
    upload = ""
    mkdir = ""
    if fs_item.get("type") == "folder":
        upload = """<div class="upload"><label for="upload">Add files to this folder:</label><input id="upload" type="file" name="upload" multiple="multiple"></div>"""
        mkdir = """<input type="submit" name="submit" value="mkdir" />"""
    #     label = "Upload files"
    label = "Add"
    edit_url = fs_item.get("url")
    edit_filename = os.path.split(edit_url.rstrip("/"))[1].replace("_"," ")

    description = item.get("description", "")
    files = "None<br>"
    if 'formats' in fs_item:
        files += """<table id="fileformat">\n"""
        formats = fs_item['formats']
        formats.sort(key=lambda x: x['size'])
        for f in formats:
            filename = os.path.split(f['url'])[1]
            files += """<tr class="file">
    <td><input type="checkbox" name="selectedformats" value="{0[url]}"></a></td>
    <td><a href="{0[url]}">{1}</a></td>
    <td>{3}</td>
    <td value="{0[size]}">{2}</td>
    </tr>\n""".format(f, filename, humanize_bytes(f['size']), f.get("type", "&mdash;"))
        files += """</table>"""

    print ("""<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>edit description</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="/lib/directory/directory.css">
</head>
<body class="edit">
<form method="post" action="" enctype="multipart/form-data">
<div id="form" class="grouper">
    <div id="description" class="group">
        <div class="title">{0[filename]}</div>
        <textarea name="description" style="width: 320px; height: 4em;" placeholder="Description" autofocus>{0[description]}</textarea>
        <div>
            <input type="submit" name="submit" value="save" />
            <input type="submit" name="submit" value="rename">
            {0[mkdir]}
            <input type="submit" name="submit" value="delete">
        </div>
        {0[messages]}
    </div>
    <div class="filelisting group">
        {0[upload]}
        <div class="files">
            <b>Versions &amp; thumbnails:</b><br>
            {0[files]}
            <select name="selectedformatsfn">
                <option></option>
                <option value="delete">delete selected</option>
            </select>
            <label for="file">Add:</label> <input id="file" type="file" name="file" multiple="multiple">
        </div>
    </div>
</div>
</form>
</body>
</html>
""".format({
    'description': description,
    'files': files,
    'filename': edit_filename,
    'messages': messages,
    'upload': upload,
    'mkdir': mkdir}))
