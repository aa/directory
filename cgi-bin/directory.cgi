#!/usr/bin/env python3
 
import cgitb; cgitb.enable()
import cgi, os, sys, json
from xml.sax.saxutils import quoteattr
from urllib.parse import quote as urlquote, unquote as urlunquote, urljoin
from os import environ
from base64 import b64encode as encode, b64decode as decode
import binascii

from directory import *
from aa_password import PASSWORD
from login import authenticate

#DEBUG
# print ("Content-type: text/html; charset=utf8")
# print ()

env = os.environ
method = os.environ.get("REQUEST_METHOD", "GET").upper()
docroot = os.environ.get("DOCUMENT_ROOT", "/var/www/html")
fs = cgi.FieldStorage()
url = fs.getvalue("u", "/")
assert(url.startswith("/"))
fullpath = os.path.join(docroot, urlunquote(url).strip("/"))
# compute normalized_url
normalized_url = urlquote(os.path.relpath(fullpath, docroot))
if normalized_url == ".":
    normalized_url = "/"
else:
    normalized_url = "/"+normalized_url
    if os.path.isdir(fullpath):
        normalized_url += "/"

# get the dispatch function (default json)
f = fs.getvalue("f", "json")
import shutil

def upload (file, pathname):
    with open (pathname, "wb") as fout:
        shutil.copyfileobj(file.file, fout)
        # while 1:
        #     chunk = file.file.read(100000)
        #     if not chunk: break
        #     bytes += len(chunk)
        #     fout.write (chunk)
    return True

def get_child_datum (data, url, create=False):
    if create and not 'children' in data:
        data['children'] = []
    if 'children' in data:
        for c in data['children']:
            if c.get("url") == url:
                return c
        if create:
            cdata = {'url': url}
            data['children'].append(cdata)
            return cdata

if f == "json":
    if not os.path.isdir(fullpath):
        print ("Content-type: application/json")
        print()
        print (json.dumps({"error": "Cannot load json of a file"}, indent=2))
        sys.exit(0)
    print ("Content-type: application/json")
    print()
    fs_data = listdir(fullpath, normalized_url)
    user_data = load_index_json(fullpath)
    merge_data(fs_data, user_data)
    print (json.dumps(fs_data, indent=2))
    # TODO MERGE WITH user json

elif f == "annotate":
    # print ("Content-type: text/html; charset=utf8")
    # print ()
    authorized = authenticate(environ)
    current_url = get_current_url(env)
    if not authorized:
        login_url = urljoin(current_url, "/cgi-bin/login.cgi")
        login_url += "?next="+urlquote(current_url)
        print ("Location: {0}".format(login_url))
        print ()
        sys.exit(0)
        # print ("Not authorized")
        # cgi.print_environ()

    messages = []
    if method=="POST":
        # print ("Content-type: text/html; charset=utf8")
        # print ()
        # HANDLE REDIRECTS
        submit = fs.getvalue('submit')
        if submit == "rename":
            redirect_to(urljoin(current_url, "/cgi-bin/rename.cgi")+"?u="+urlquote(normalized_url))
        elif submit == "delete":
            redirect_to(urljoin(current_url, "/cgi-bin/delete.cgi")+"?u="+urlquote(normalized_url))
        elif submit == "mkdir":
            redirect_to(urljoin(current_url, "/cgi-bin/mkdir.cgi")+"?u="+urlquote(normalized_url)) 

        description = fs.getvalue("description")

        #if os.path.isfile(fullpath):
        # HANDLE UPLOADS
        # FORMATS
        files = fs["file"]
        if not isinstance(files, list):
            files = [files]
        files = [x for x in files if x.filename]
        if files:
            # these get added as "formats" to the file
            formatpath = file_formats_path(fullpath, ensureCreate=True)
            # print ("files {0}".format(len(files)))
            results = []
            for f in files:
                savepath = os.path.join(formatpath, f.filename)
                count = upload(f, savepath)
                results.append((f.filename, os.path.getsize(savepath)))
                messages.append("Uploaded {0}".format(savepath))
            messages.append("Uploaded {0} files".format(len(files)))

        # UPLOADS (directory)
        try:
            files = fs["upload"]
            if not isinstance(files, list):
                files = [files]
            files = [x for x in files if x.filename]
            if os.path.isdir(fullpath):
                if files:
                    results = []
                    for f in files:
                        savepath = os.path.join(fullpath, f.filename)
                        count = upload(f, savepath)
                        results.append((f.filename, os.path.getsize(savepath)))
                        messages.append("Uploaded {0}".format(savepath))
                    messages.append("Uploaded {0} files".format(len(files)))
        except KeyError:
            pass

        # HANDLE DELETIONS (selectedformats)
        sff = fs.getvalue("selectedformatsfn")
        if sff == "delete":
            for sf_url in fs.getlist("selectedformats"):
                sf_fullpath = os.path.join(docroot, urlunquote(sf_url).strip("/"))
                if os.path.exists(sf_fullpath):
                    os.remove(sf_fullpath)
                    messages.append("deleted {0}".format(sf_url))
                else:
                    messages.append("file not found {0}".format(sf_url))

    # DEBUG
    # print ("Content-type: text/html; charset=utf8")
    # print ()

    if os.path.isfile(fullpath):
        parentpath = os.path.split(fullpath)[0]
        user_data_path = index_json_path(parentpath)
        user_data = load_index_json(parentpath)
        item = get_child_datum(user_data, normalized_url, create=True)
        fs_data = listdir(parentpath, os.path.split(normalized_url.rstrip("/"))[0]+"/")
        fs_item = get_child_datum(fs_data, normalized_url)
    else:
        user_data_path = index_json_path(fullpath)
        user_data = load_index_json(fullpath)
        item = user_data
        fs_data = listdir(fullpath, normalized_url)
        fs_item = fs_data
    # nb: user_data remains unchanged (what gets (potentially) stored below)
    merge_data(fs_data, user_data)

    if method=="POST":
        # print ("Content-type: text/html; charset=utf8")
        # print ()
        # APPLY DATA AND SAVE BACK IF CHANGED
        changed = False
        if description and (item.get("description") != description):
            item['description'] = description
            changed = True
        if changed:
            try:
                os.makedirs(os.path.split(user_data_path)[0])
            except OSError:
                pass
            with open(user_data_path, "w") as f:
                json.dump(user_data, f)
            messages.append("Updated item")


    if messages:
        messages = "\n".join(["""<div class="message">{0}</div>""".format(msg) for msg in messages])
        messages = """<div id="messages">{0}</div>""".format(messages)
    else:
        messages = ""

    print ("Content-type: text/html; charset=utf8")
    print ()
    # nb fs_item represents the merged version
    send_form(item, fs_item, messages)
    # APPLY NEW DATA TO ITEM
    # save the json
    # evt. RECEIVE FILES FOR ITEM
    # record data for them (data is considered "bulk" labels for each received file?)

# parent, filename = os.path.split(fullpath)
# aadir = os.path.join(parent, ".aa")
# filepath = os.path.join(aadir, filename)



