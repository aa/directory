#!/usr/bin/env python3

import cgi
# import cgitb; cgitb.enable()
from os import environ
from base64 import b64encode as encode, b64decode as decode
import sys
from login import authenticate
from aa_password import PASSWORD


if __name__ == "__main__":
    method = environ.get("REQUEST_METHOD", "GET").upper()
    fs = cgi.FieldStorage()
    authorized = authenticate(environ)
    formclass = ""

    if method == "POST":
        logout = fs.getvalue("logout")
        if logout:
            print ("Set-Cookie: aa_auth=; path=/; max-age:0")
            authorized = False
        elif not authorized: 
            # attempt to login
            password = fs.getvalue("door")
            if password == PASSWORD:
                # Set the cookie and redirect to next (if present)
                password = encode(PASSWORD.encode("utf-8")).decode("utf-8")
                print ("Set-Cookie: aa_auth={0};path=/".format(password))
                authorized = True
            else:
                formclass="error"

    if not authorized:
        print ("Content-type:text/html; charset=utf-8")
        print ()
        print ("""<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>login</title>
<style>
p {{
    max-width: 32em;
    text-align: center;
}}
form {{
    margin-top: 1em;
    text-align: center;
}}
form.error input {{
    border: 2px solid red;
}}
</style>
</head>
<body>
<p>
<form method="post" action="" class="{0[class]}">
<input id="door" type="password" name="door" value="" placeholder="login with password" autofocus>
</form>
</p>
</body>
</html>
""".format({
        "class": formclass
    }))
    else:
        nexturl = fs.getvalue("next")
        if method=="POST" and nexturl:
            print ("Location: {0}".format(nexturl))
            print ()
            sys.exit(0)
        print ("Content-type:text/html; charset=utf-8")
        print ()
        print ("""<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>login</title>
</head>
<body>
<p><b>You are logged in.</b></p>
<form method="post" action="">
<input type="submit" name="logout" value="logout" />
</form>
</body>
</html>
""")

