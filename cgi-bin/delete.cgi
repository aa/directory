#!/usr/bin/env python3
 
import cgitb; cgitb.enable()
import cgi, os, sys, json
from xml.sax.saxutils import quoteattr
from urllib.parse import quote as urlquote, unquote as urlunquote, urljoin
from os import environ
from base64 import b64encode as encode, b64decode as decode
import binascii

from directory import *
from aa_password import PASSWORD
from login import authenticate

#DEBUG


env = os.environ
method = os.environ.get("REQUEST_METHOD", "GET").upper()
docroot = os.environ.get("DOCUMENT_ROOT", "/var/www/html")
fs = cgi.FieldStorage()
url = fs.getvalue("u", "/")
assert(url.startswith("/"))
fullpath = os.path.join(docroot, urlunquote(url).strip("/"))
# compute normalized_url
normalized_url = urlquote(os.path.relpath(fullpath, docroot))
if normalized_url == ".":
    normalized_url = "/"
else:
    normalized_url = "/"+normalized_url
    if os.path.isdir(fullpath):
        normalized_url += "/"


authorized = authenticate(environ)
if not authorized:
    current_url = get_current_url(env)
    login_url = urljoin(current_url, "/cgi-bin/login.cgi")
    login_url += "?next="+urlquote(current_url)
    print ("Location: {0}".format(login_url))
    print ()
    sys.exit(0)

if method=="POST":
    submit = fs.getvalue("submit")
    if submit == "commit":
        ffp = file_formats_path(fullpath)
        if os.path.isdir(ffp):
            for x in os.listdir(ffp):
                ffp2 = os.path.join(ffp, x)
                os.remove(ffp2)
            os.rmdir(ffp)
        if os.path.isdir(fullpath):
            aapath = os.path.join(fullpath, ".aa")
            if os.path.isdir(aapath):
                os.rmdir(aapath)
            os.rmdir(fullpath)
        else:
            os.remove(fullpath)
        print ("Content-type: text-html;charset=utf-8")
        print ()
        print ("Deleted {0}".format(normalized_url))
        # parent = os.path.split(normalized_url)[0]
        # current_url = get_current_url(env)
        # redirect_url = urljoin(current_url, "/cgi-bin/directory.cgi")+"?f=annotate&u="+urlquote(parent)
        # print ("Location: {0}".format(redirect_url))
        # print ()
        sys.exit(0)


print ("Content-type: text/html; charset=utf8")
print ()
# nb fs_item represents the merged version
print("""<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>edit description</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="/lib/directory/directory.css">
</head>
<body class="edit">
<form method="post" action="">
<p>Delete {0} and any and all related versions / thumbnails?</p>
Are you sure?
<input type="submit" name="submit" value="commit">
</form>
</body>
</html>
""".format(normalized_url))