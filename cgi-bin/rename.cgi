#!/usr/bin/env python3
 
import cgitb; cgitb.enable()
import cgi, os, sys, json
from xml.sax.saxutils import quoteattr
from urllib.parse import quote as urlquote, unquote as urlunquote, urljoin
from os import environ
from base64 import b64encode as encode, b64decode as decode
import binascii

from directory import *
from aa_password import PASSWORD
from login import authenticate

#DEBUG
# print ("Content-type: text/html; charset=utf8")
# print ()


env = os.environ
method = os.environ.get("REQUEST_METHOD", "GET").upper()
docroot = os.environ.get("DOCUMENT_ROOT", "/var/www/html")
fs = cgi.FieldStorage()
url = fs.getvalue("u", "/")
assert(url.startswith("/"))
fullpath = os.path.join(docroot, urlunquote(url).strip("/"))
# compute normalized_url
normalized_url = urlquote(os.path.relpath(fullpath, docroot))
if normalized_url == ".":
    normalized_url = "/"
else:
    normalized_url = "/"+normalized_url
    if os.path.isdir(fullpath):
        normalized_url += "/"


authorized = authenticate(environ)
if not authorized:
    current_url = get_current_url(env)
    login_url = urljoin(current_url, "/cgi-bin/login.cgi")
    login_url += "?next="+urlquote(current_url)
    print ("Location: {0}".format(login_url))
    print ()
    sys.exit(0)

if method=="POST":
    description = fs.getvalue("description")


print ("Content-type: text/html; charset=utf8")
print ()
# nb fs_item represents the merged version
print("""
Coming soon!
""")