
```
     ___             __               
 ___/ (_)______ ____/ /____  ______ __
/ _  / / __/ -_) __/ __/ _ \/ __/ // /
\_,_/_/_/  \__/\__/\__/\___/_/  \_, / 
                               /___/  

```

Goal: to build a simple (as possible) means of convering standard Apache directory listings into a local file-sharing repository. The results are accessible via a feed API.

## Deployment on Apache

```
IndexHeadInsert "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"><script type=\"text/javascript\" src=\"/lib/directory/directory
.js\"></script><link rel=\"stylesheet\" type=\"text/css\" href=\"/lib/directory/directory.css\">"


HeaderName /include/HEADER.shtml
ReadmeName README.html

```


## Resources

* <https://github.com/marcj/css-element-queries> Great shim to support responsive elements

## YAGNI?

* https://photoswipe.com/documentation/custom-html-in-slides.html
* https://lokeshdhakar.com/projects/lightbox2/
* https://developer.mozilla.org/en-US/docs/Web/CSS/pointer-events
* the ever useful: https://css-tricks.com/snippets/css/a-guide-to-flexbox/
